package pl.sda.studentslog;

import java.util.*;

public class StudentMarks {
    private Map<Subject, List<Integer>> markMap=new HashMap<>();


    public void addMark(Subject subject, Integer mark) {

        List<Integer> subjectMarks = markMap.get(subject);
        if (subjectMarks == null) {
            subjectMarks = new ArrayList<>();
        }
        subjectMarks.add(mark);
        markMap.put(subject, subjectMarks);
    }

    public List<Integer> getMarksOfSubject(Subject subject) {
        return markMap.get(subject);
    }

    public Double calculateAverage() {
        OptionalDouble averageOfMarks = markMap.values()
                .stream()
                .mapToDouble(list -> list.stream()
                        .mapToInt(e -> e).average()
                        .getAsDouble())
                .average(); // <- calculate average of all subjects


        return averageOfMarks.getAsDouble();
    }




    public Map<Subject, List<Integer>> getMarkMap() {
        return markMap;
    }
}
