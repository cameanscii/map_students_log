package pl.sda.studentslog;


import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SchoolLog {

    private Map<String,StudentData> studentsMap=new HashMap<String, StudentData>();


    public void addStudent(String studentID, String firstName, String lastName){
        studentsMap.put(studentID, new StudentData(studentID,firstName,lastName));
    }

    public void addStudentMark(String studentID,Subject subject, Integer mark){
        StudentData studentData=studentsMap.get(studentID);

        if(studentData!=null){
            studentData.getStudentMarks().addMark(subject,mark);
        }else {
            throw new IllegalArgumentException("Student with this ID doesn't exist");
        }
    }

    public double returnAverage(String studentId) {
        return studentsMap.get(studentId).getStudentMarks().calculateAverage();
    }

    public void getOceny(String studentId) {
        StudentMarks studentMarks = studentsMap.get(studentId).getStudentMarks();
        for (Map.Entry<Subject, List<Integer>> marksFromSpecificSubject : studentMarks.getMarkMap().entrySet()) {
            // Class entry has mthod getKey ( Subject )
            // Class entry has mthod getValue ( List<Integer>)
            System.out.println("" + marksFromSpecificSubject.getKey() + " : " + marksFromSpecificSubject.getValue());
        }
    }

}
